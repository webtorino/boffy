<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

require_once('./LINEBotTiny.php');

$channelAccessToken = 'dedvZAIedAuaNA1qx2COkrm1notlvC7fg7/snIpnYN7rS/qWqOB3tbTGVnr0eLAjZLZ8M7/rGKG87ncxcSpeVbYmDXpzz/uLDy+o9GwyBMYLSwdTSRbsCrGsMxfMc/+4wX9DtoFP64GbsdDxTnzNDQdB04t89/1O/w1cDnyilFU=';
$channelSecret = '6d4bfb0875a341f17520981c924cb07b';

$pattern = '/(php)[\s\w]*(language)/i';
$rep_type='text';
$ojigame=array(
    "「マルコポーロの旅路」",
    "「ルイスクラーク探検隊」",
    "「ワトソン&ホームズ」",
    "「グレート・ウエスタン・トレイル」"
);
$omikuji=array(
    "う～ん…大吉！",
    "はい！吉です！",
    "うわああ凶だあ～～～\nぼくを撫でれば大吉になるよ！",
    "おめでとう！大吉だよ！",
    "そうだなあ、半吉ってとこかな！",
    "んーとね、中吉だね。",
    "はい、勝ち確大吉あげる！！！"
);

$userAr=array(
    "",
    "torino",
    "namachan",
    "ebi",
    "ayanoko",
    "arinko",
    "kikutaro",
    "2l",
    "hana",
    "nohe"
);


$client = new LINEBotTiny($channelAccessToken, $channelSecret);
foreach ($client->parseEvents() as $event) {
    switch ($event['type']) {
        case 'message':
            $message = $event['message'];
            switch ($message['type']) {
                
                case 'text':
                    

                //発言者を特定
                $txtfile1 = file_get_contents("./data/member.txt");
                $txtfile1 = mb_convert_encoding($txtfile1,"utf-8","auto");
                $MemberArray = explode("＆",$txtfile1);
                
                $user_i = 0;
                //ユーザーのユニークID
                 $userid=$event['source']['userId'];
                
                foreach($MemberArray as $key => $member){
                    if($useri < count($MemberArray)){
                        $MemberArray[$user_i] = explode(",",$MemberArray[$user_i]);
                            if(preg_match("/".$userid."/",$MemberArray[$user_i][0])){
                            $hatsugensha=$MemberArray[$user_i][1];
                            $hatsugenshaID=$MemberArray[$user_i][0];
                            $userBangou=$user_i;
                        }
                    }
                    $user_i++;
                  }


                  

                    
        
                  
$pattern = '/名前をおぼえて！/';
if (preg_match($pattern, $message['text'] )){
  $rep = "きみはだあれ？";
 rep_name1($client,$event,$rep);
        }

 $pattern = '/この中にはいません/';
        if (preg_match($pattern, $message['text'] )){
          $rep = "この中にはいる？";
         rep_name2($client,$event,$rep);
                }

$pattern = '/この中にはいないよ/';
if (preg_match($pattern, $message['text'] )){
  $rep = "じゃあきみはこの辺の誰かだね";
 rep_name3($client,$event,$rep);
        }

       

                //▼名前とユニークIDを紐づけ
                $pattern = '/これから「.*?」って呼んで/';
                if (preg_match($pattern, $message['text'] )){

                    preg_match('/「(.*)」/', $message['text'], $namedate) ;

 $txtfile = file_get_contents("./data/member.txt");
$txtfile = mb_convert_encoding($txtfile,"utf-8","auto");
$txtarray = explode("＆",$txtfile);

$i = 0;
//ユーザーのユニークID
 $id=$event['source']['userId'];
$id_tourokuFlag=false;

foreach($txtarray as $key => $member){
    if($i < count($txtarray)){
        $txtarray[$i] = explode(",",$txtarray[$i]);
            if(preg_match("/".$id."/",$txtarray[$i][0])){
            $id_tourokuFlag=true;
            $tourokuzumiID=$i;
        }
        
    }
    $i++;
  }

  if($id_tourokuFlag == true){
    //登録済み
    if($namedate[1]==$txtarray[$tourokuzumiID][1]){
    $rep='うんうん、'.$txtarray[$tourokuzumiID][1].'。覚えてるよ！';

    }else{

        $rep='あれぇ。きみは…'.$txtarray[$tourokuzumiID][1].'だった気がするけど…'.$namedate[1]."だったんだね！ごめんごめん、覚えたよ！";
    
//名前の上書き
 $txtfile = file_get_contents("./data/member.txt");
 $txtfile = mb_convert_encoding($txtfile,"utf-8","auto");
 $txtarray = explode("＆",$txtfile);

 $uwagakiTxt="";

 $i = 0;
foreach($txtarray as $key => $member){
    if($i < count($txtarray)){

        $txtarray[$i] = explode(",",$txtarray[$i]);
            if(preg_match("/".$id."/",$txtarray[$i][0])){
                $txtarray[$i][1]=$namedate[1];
        }
        if($i!=0){
            $uwagakiTxt.="＆";
        }
        $uwagakiTxt.=$txtarray[$i][0].",".$txtarray[$i][1].",".$txtarray[$i][2];
    }
    $i++;
  }

$uwagakifile = fopen("./data/member.txt", "w");
@fwrite($uwagakifile, $uwagakiTxt);
fclose($uwagakifile);   

//$rep=$uwagakiTxt;
}
  }
  else{
    //新規ユーザー登録
    $name=$namedate[1];
    $boffy_love=0;
    $newid=$event['source']['userId'];

    //新規ユーザー登録書き込み
 $a = fopen("./data/member.txt", "a");
 @fwrite($a, "＆".$newid.",".$name.",".$boffy_love);
 fclose($a);
 $rep='ふむふむ。'.$namedate[1].'だね。ボッフィー覚えた';

  }

                     //配列の内容を出力
                   //  $rep=$filearr;
                    //  foreach ($filearr as $no => $val) {
                    //     $rep= $val . "\n";
                    //  }




                    rep_text($client,$event,$rep);
                }
                



                    $pattern = '/(イカ|スプラ).*?ステージ/';
                    if (preg_match($pattern, $message['text'] )){
                        $data1 = "https://spla2.yuu26.com/regular/now";
                        $data2 = "https://spla2.yuu26.com/gachi/now";
                        $data3 = "https://spla2.yuu26.com/league/now";
$json1 = file_get_contents($data1);
$json2 = file_get_contents($data2);
$json3 = file_get_contents($data3);
                        
$json_decode1 = json_decode($json1, false);
$json_decode2 = json_decode($json2, false);
$json_decode3 = json_decode($json3, false);
                        
$rep = "今のナワバリは\n".$json_decode1->result[0]->maps[0]."と".$json_decode1->result[0]->maps[1]."だよ\n";  
$rep .= "ガチマは".$json_decode2->result[0]->rule."で\n".$json_decode2->result[0]->maps[0]."と".$json_decode2->result[0]->maps[1]."だよ\n"; 
$rep .= "リグマは".$json_decode3->result[0]->rule."で\n".$json_decode3->result[0]->maps[0]."と".$json_decode3->result[0]->maps[1]."だよ\n";   
$rep .= "ぼくもみんなとスプラトゥーンしたいなあ！";   
rep_text($client,$event,$rep);            
                    }
                    $pattern = '/ガチマ.*?(ステージ|なに|何|おしえて|教えて|どこ)/';
                    if (preg_match($pattern, $message['text'] )){
                        $data2 = "https://spla2.yuu26.com/gachi/now";
$json2 = file_get_contents($data2);
$json_decode2 = json_decode($json2, false);
                        
$rep = "今のガチマは".$json_decode2->result[0]->rule."で\n".$json_decode2->result[0]->maps[0]."と".$json_decode2->result[0]->maps[1]."だよ\n"; 
$rep .= "がんばれ～！応援してるよ！";
rep_text($client,$event,$rep);               
                    }
                    $pattern = '/リグ.*?(ステージ|なに|何|おしえて|教えて|どこ)/';
                    if (preg_match($pattern, $message['text'] )){
                        $data3 = "https://spla2.yuu26.com/league/now";
$json3 = file_get_contents($data3);
$json_decode3 = json_decode($json3, false);
                        
$rep = "今のリグマは".$json_decode3->result[0]->rule."で\n".$json_decode3->result[0]->maps[0]."と".$json_decode3->result[0]->maps[1]."だよ\n"; 
$rep .= "ぼくも混ぜて～っ！";  
rep_text($client,$event,$rep);             
                    }
                    $pattern = '/ナワバリ.*?(ステージ|なに|何|おしえて|教えて|どこ)/';
                    if (preg_match($pattern, $message['text'] )){
                        $data1 = "https://spla2.yuu26.com/regular/now";
$json1 = file_get_contents($data1);
$json_decode1 = json_decode($json1, false);
                        
$rep = "今のナワバリは\n".$json_decode1->result[0]->maps[0]."と".$json_decode1->result[0]->maps[1]."だよ！\n";   
$rep .= "たくさん塗ろうね！";   
rep_text($client,$event,$rep);            
                    }
                    
                    
                    
                    //▼ここからしょうもない会話用
                    $pattern = '/(ありがとう|Thank you|サンキュー).*?ボッフィー/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "どういたしまして！".$hatsugensha."！";
                                rep_text($client,$event,$rep);
                            }


                            $pattern = '/声出せや/';
                            if (preg_match($pattern, $message['text'] )){
                                $voiceURL = "https://pichan.net/boffy/mp4/".$userAr[$userBangou].".mp4";
                                $rep="コホン…";
                                rep_voice($client,$event,$rep,$voiceURL);
                            }

                    $pattern = '/ボッフィー/';
                    if (preg_match($pattern, $message['text'] )){
                        $ranarray=array(
                            "こんにちは！ぼくはボッフィー！",
                            "誰かぼくのこと呼んだ？",
                            "ぼくこそがボッフィー！",
                            "ぼくはボ会のマスコットキャラクターのボッフィー！やさしくなでてね"
                        ); 
                        $ranarray_nakayosi=array(
                            $hatsugensha."、僕を呼んだ？",
                            "ヤッホー".$hatsugensha."！今日も輝いてるね！",
                            $hatsugensha."～！どうしたの？",
                            $hatsugensha."の声が聞けてうれしいなあ！"
                        );
                                $rep = $ranarray[mt_rand(0, 3)];

                             if($hatsugensha){

                                 
                                $voiceURL = "https://pichan.net/boffy/mp4/".$userAr[$userBangou].".mp4";
                                $rep=$ranarray_nakayosi[mt_rand(0, 3)];
                                rep_voice($client,$event,$rep,$voiceURL);
                             }else{

                                rep_text($client,$event,$rep);}
                            }
                       
                  $toshiyuki = mt_rand(0, 5);     
                  if($toshiyuki==1){
                    $pattern = '/としゆき/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "としゆき！";
                                $voiceURL = "https://pichan.net/boffy/mp4/toriyuki.mp4";
                                rep_voice($client,$event,$rep,$voiceURL);
                            }
                        } 
                   if($toshiyuki==2){
                     $pattern = '/マイケル/';
                     if (preg_match($pattern, $message['text'] )){
                                 $rep = "マイケル！";
                                 rep_text($client,$event,$rep);
                             }
                         }


                        $bo0223="2/23（土）\n場所：小屋（坂下二丁目集会所）\n参加予定：あやのこ、に～える、\nとりの、きくたろ\nだよ！２週連続よくばりボ会だよ！"; 
                        $bo0302="3/2（土）\n場所：志村坂上地域センター\n参加予定：あやのこ、に～える、\nとりの、きくたろ、じぇい\n参加者お待ちしてま～す！";    
                          
                   
                             $pattern = '/次.*?(ボドゲ会|ボ会)/';
                             if (preg_match($pattern, $message['text'] )){
                                // $rep=$nextbokai;
                               $rep = "今後のボドゲ会はこうなってま～す！";
                              rep_nextbodoge($client,$event,$rep);
                                         // rep_text($client,$event,$rep);
                                     }


                          

                              $pattern = '/2月23日のボドゲ会の詳細が知りたい！/';
                              if (preg_match($pattern, $message['text'] )){
                                          $rep = "よしきた！\n\n".$bo0223;
                                          rep_text($client,$event,$rep);
                                      }

                              $pattern = '/3月2日のボドゲ会の詳細が知りたい！/';
                              if (preg_match($pattern, $message['text'] )){
                                          $rep = "ほ～い！\n\n".$bo0302;
                                          rep_text($client,$event,$rep);
                                      }

                          


                    $pattern = '/小屋.*?どこ/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "ぼくらの小屋はここだよ！\n
https://www.google.co.jp/maps?q=%E5%9D%82%E4%B8%8B%E4%BA%8C%E4%B8%81%E7%9B%AE%E9%9B%86%E4%BC%9A%E6%89%80&um=1&ie=UTF-8&sa=X&ved=0ahUKEwjvwNWzpOzdAhWOAnwKHT8-BnoQ_AUICigB";
rep_text($client,$event,$rep);
                            }
                    $pattern = '/ダッフィー/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "ぼくはボ会のマスコットキャラクターのボッフィーだよ…？";
                    $img_url='https://pichan.net/boffy/rep_img/duffy.png';
                    $rep_type='image';
                    rep_img($client,$event,$rep,$img_url);
                            }
                    $pattern = '/カンガルー/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "かつてのぼく…なんちゃって！\nhttps://www.youtube.com/watch?v=FIRT7lf8byw";
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/向かってる|むかってる/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "慌てなくても大丈夫だよ！待ってるね！";
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/おはよう/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "おはよー！";
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/ソイ(の|が|を|で|チューバー)/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "✝Boffy✝";
                    $img_url='https://pichan.net/boffy/rep_img/soy.jpg';
                    $rep_type='image';
                    rep_img($client,$event,$rep,$img_url);
                            }
                    $pattern = '/おじ/';
                    if (preg_match($pattern, $message['text'] )){
                       $valRandFunc = mt_rand(0, count($ojigame));
                                $rep = "今日のラッキーおじゲーは".$ojigame[$valRandFunc]."!";
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/遅れ/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "了解！遅れても大丈夫だよ！ぼくからみんなに伝えておくね！";
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/おみくじ/';
                    if (preg_match($pattern, $message['text'] )){
                       $valRandFunc = mt_rand(0, count($omikuji)-1);
                       if($hatsugensha){
                        $rep =  $hatsugensha."のためにおみくじ引くね～！\n\n\n".$omikuji[$valRandFunc];
                       }else{
                        $rep = "おみくじ引くね～！\n\n\n".$omikuji[$valRandFunc];
                       }
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/揉む|なでなで/';
                    if (preg_match($pattern, $message['text'] )){
                                $rep = "やめて！乱暴しないで！";
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/(ほ|褒|誉)めて/';
                    if (preg_match($pattern, $message['text'] )){
                    $homete=array(
                        "さすが～！",
                        "知らなかった～！",
                        "すごーい！",
                        "センスある～！",
                        "そうなんだあ～"
                    );
                       $valRandFunc = mt_rand(0, count($homete));
                                $rep = $homete[$valRandFunc];
                                rep_text($client,$event,$rep);
                            }
                    $pattern = '/(なぐさ|慰)めて|(かな|悲)しい|(つか|疲)れた/';
                    if (preg_match($pattern, $message['text'] )){                        
                    $homete=array(
                        "なにかあった？".$hatsugensha."がんばれがんばれ！きみはえらい！ぼくもえらい！",
                        "大丈夫？ボッフィー揉む？",
                        "スリスリ。ぼくのヒーリング効果をくらえ！"
                    );
                       $valRandFunc = mt_rand(0, count($homete));
                                $rep = $homete[$valRandFunc];
                                rep_text($client,$event,$rep);
                            }

                    $pattern = '/(wwww|ｗｗｗ)/';
                    if (preg_match($pattern, $message['text'] )){                      
                    $wara=array(
                        "てんさいｗｗｗ",
                        "おもしろｗｗｗｗ",
                        "さいこうｗｗｗｗ"
                    );
                       $valRandFunc = mt_rand(0, count($wara)-1);
                                $rep = $hatsugensha.$wara[$valRandFunc];
                                rep_text($client,$event,$rep);
                            }
                

            }
            break;
        default:
            error_log("Unsupporeted event type: " . $event['type']);
            break;
    }
    
};

function rep_text($client,$event,$rep)
{
    BPup($client,$event,$rep);
    $client->replyMessage(array(
        'replyToken' => $event['replyToken'],
        'messages' => array(
            array(
                'type' => 'text',
                'text' => $rep
            )
        )
    ));
}

function rep_voice($client,$event,$rep,$voiceURL)
{
   

    $client->replyMessage(
        array(
        'replyToken' => $event['replyToken'],
        'messages' => array(array(
            'type' => 'text',
            'text' => $rep
        ),array(
                'type' => 'video',
                'originalContentUrl' => $voiceURL,
                'previewImageUrl'    => 'https://pichan.net/boffy/rep_img/voice.jpg'
        )
        )
    ));
}


/*

           
            array(
                'type' => 'text',
                'text' => $voiceURL
            )
 */

function rep_img($client,$event,$rep,$img_url)
{
    BPup($client,$event,$rep);
    $client->replyMessage(array(
        'replyToken' => $event['replyToken'],
        'messages' => array(
            array(
               'type'               => 'image',
               'originalContentUrl' => $img_url,
               'previewImageUrl'    => $img_url
            ),
            array(
                'type' => 'text',
                'text' => $rep
            )
        )
    ));
}

function BPup($client,$event,$rep){
    //ボッフィーポイントの加算
 $txtfile = file_get_contents("./data/member.txt");
 $txtfile = mb_convert_encoding($txtfile,"utf-8","auto");
 $txtarray = explode("＆",$txtfile);

 $uwagakiTxt="";

 $i = 0;
foreach($txtarray as $key => $member){
    if($i < count($txtarray)){

        $txtarray[$i] = explode(",",$txtarray[$i]);
            if(preg_match("/".$event['source']['userId']."/",$txtarray[$i][0])){
                $txtarray[$i][2]=$txtarray[$i][2]+1;
        }
        if($i!=0){
            $uwagakiTxt.="＆";
        }
        $uwagakiTxt.=$txtarray[$i][0].",".$txtarray[$i][1].",".$txtarray[$i][2];
    }
    $i++;
  }

$uwagakifile = fopen("./data/member.txt", "w");
@fwrite($uwagakifile, $uwagakiTxt);
fclose($uwagakifile);   
}


function rep_nextbodoge($client,$event,$rep)
{
    BPup($client,$event,$rep);
    $template = array('type'    => 'buttons',
    'text'    => $rep,
    'actions' => array(
                   array('type'=>'message', 'label'=>'2月23日（土）小屋',  'text'=>'2月23日のボドゲ会の詳細が知りたい！' ),
                   array('type'=>'message', 'label'=>'3月2日（土）地域センター',  'text'=>'3月2日のボドゲ会の詳細が知りたい！' )
                  )
  );


  $client->replyMessage(array(
    'replyToken' => $event['replyToken'],
    'messages' => array(
        array(
           'type'               => 'template',
           'altText'  => $rep,
           'text'  => $rep,
           'thumbnailImageUrl'=> 'https://pichan.net/boffy/rep_img/bodoge.jpg',
           'template' => $template
        )
    )
));
}



function rep_name1($client,$event,$rep)
{
    $template = array('type'    => 'buttons',
    'text'    => $rep,
    'actions' => array(
        array('type'=>'message', 'label'=>'あやのこ',  'text'=>'これから「あやのこ」って呼んで！' ),
        array('type'=>'message', 'label'=>'とりの',  'text'=>'これから「とりの」って呼んで！' ),
        array('type'=>'message', 'label'=>'に～える',  'text'=>'これから「に～える」って呼んで！' ),
        array('type'=>'message', 'label'=>'次',  'text'=>'この中にはいません' )
                  )
  );


  $client->replyMessage(array(
    'replyToken' => $event['replyToken'],
    'messages' => array(
        array(
           'type'               => 'template',
           'altText'  => $rep,
           'text'  => $rep,
           'template' => $template
        )
    )
));
}




function rep_name2($client,$event,$rep)
{
    $template = array('type'    => 'buttons',
    'text'    => $rep,
    'actions' => array(
        array('type'=>'message', 'label'=>'きくたろ',  'text'=>'これから「きくたろ」って呼んで！' ),
        array('type'=>'message', 'label'=>'はな',  'text'=>'これから「はな」って呼んで！' ),
        array('type'=>'message', 'label'=>'のへ',  'text'=>'これから「のへ」って呼んで！' ),
        array('type'=>'message', 'label'=>'次',  'text'=>'この中にはいないよ' )
                  )
  );


  $client->replyMessage(array(
    'replyToken' => $event['replyToken'],
    'messages' => array(
        array(
           'type'               => 'template',
           'altText'  => $rep,
           'text'  => $rep,
           'template' => $template
        )
    )
));
}


function rep_name3($client,$event,$rep)
{
    $template = array('type'    => 'buttons',
    'text'    => $rep,
    'actions' => array(
                   array('type'=>'message', 'label'=>'なまちゃん',  'text'=>'これから「なまちゃん」って呼んで！' ),
                   array('type'=>'message', 'label'=>'ありんこ',  'text'=>'これから「ありんこ」って呼んで！' ),
                   array('type'=>'message', 'label'=>'えび',  'text'=>'これから「えび」って呼んで！' ),
                   array('type'=>'message', 'label'=>'最初に戻る',  'text'=>'名前をおぼえて！' )
                  )
  );


  $client->replyMessage(array(
    'replyToken' => $event['replyToken'],
    'messages' => array(
        array(
           'type'               => 'template',
           'altText'  => $rep,
           'text'  => $rep,
           'template' => $template
        )
    )
));
}

